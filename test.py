from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import csv

# Netlify

chrome_options = webdriver.ChromeOptions()
chrome_options.binary_location = '/usr/bin/chromium-browser'
chrome_options.add_argument('--headless')
driver = webdriver.Chrome(options=chrome_options)

'''
megaescrapin ipermegared
version: me cago en la puta

pasa la url
toma la data
'''

base_url = "https://fitia.app/calorias-informacion-nutricional/milkshake-de-canela-vegano-"
# el faubri vio que el numero es el paginador

start_number = 1009201
end_number = 1009661
recipe_titles = [str(num) for num in range(start_number, end_number + 1)]

with open('scraped_data.csv', 'w', newline='', encoding='utf-8') as csvfile:
	fieldnames = ['Titulo', 'Imagen', 'Ingrediente', 'Instrucciones', 'Calorias', 'Grasas', 'Carbohidratos', 'Proteinas']
	writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
	writer.writeheader()

	for title in recipe_titles:
		url = base_url + title
		driver.get(url)
		time.sleep(1)

		data = {}

		data['Titulo'] = driver.find_element(By.CSS_SELECTOR, 'h1.title-2').text.strip()
		data['Imagen'] = driver.find_element(By.CSS_SELECTOR, 'img.rounded-lg').get_attribute('src')

		# extrae ingredientes y almacena en una lista
		ingredient_name_elements = driver.find_elements(By.CSS_SELECTOR, '.flex.items-center.space-x-4 span')
		ingredient_amount_elements = driver.find_elements(By.CSS_SELECTOR, '.py-4.text-sm')

		ingredient_list = []

		for name_element, amount_element in zip(ingredient_name_elements, ingredient_amount_elements):
			ingredient_name = name_element.text
			ingredient_amount = amount_element.text.split('\n')[1]
			ingredient_list.append(f"{ingredient_name} {ingredient_amount}")

		data['Ingrediente'] = ingredient_list


		# extrae instrucciones y almacena en una lista
		instruction_elements = driver.find_elements(By.CSS_SELECTOR, '.flex.flex-col.divide-y.divide-gray-300 .flex.items-center.py-4.space-x-3.md\:space-x-4.text-sm.sm\:text-base')
		instruction_list = []

		for element in instruction_elements:
			step_number = element.find_element(By.CSS_SELECTOR, '.subtitle-2.font-bold').text
			step_description = element.find_element(By.CSS_SELECTOR, 'div:nth-child(2) p').text
			instruction_list.append(f"{step_number}. {step_description}")

		data['Instrucciones'] = instruction_list

		nutrition_elements = driver.find_elements(By.CSS_SELECTOR, '.grid.grid-cols-1.place-content-evenly .flex.flex-col.items-center.space-y-1.rounded-xl.p-3.shadow-uniform')

		calories = nutrition_elements[0].find_element(By.CSS_SELECTOR, '.title-3.font-bold').text
		fats = nutrition_elements[1].find_element(By.CSS_SELECTOR, '.title-3.font-bold').text
		carbohydrates = nutrition_elements[2].find_element(By.CSS_SELECTOR, '.title-3.font-bold').text
		proteins = nutrition_elements[3].find_element(By.CSS_SELECTOR, '.title-3.font-bold').text

		data['Calorias'] = calories
		data['Grasas'] = fats
		data['Carbohidratos'] = carbohydrates
		data['Proteinas'] = proteins

		# guarda todo en un arreglo

		writer.writerow(data)

driver.quit()

print("Datos raspados guardados en scraped_data.csv")
